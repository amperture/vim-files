set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/NERDTree'
Plugin 'jceb/vim-orgmode'
Plugin 'tpope/vim-speeddating'
Plugin 'utl.vim'
Plugin 'mattn/calendar-vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"execute pathogen#infect()

syntax on
filetype plugin indent on
autocmd vimenter * NERDTree
autocmd vimenter * wincmd p


map <C-n> :NERDTreeToggle<CR>


"Display cursor on last line of the screen or in the status line of a window
set ruler


"Display Red Barrier at Column 79
set columns=79
set colorcolumn=79

set nu
syntax on

"Saving: Whether in normal, insert, or visual modes, ALT-S saves the current document.
nnoremap <M-s> :update<CR>
vnoremap <M-s> <C-c>:update<CR>
inoremap <M-s> <C-c>:update<CR>

"Indentation: Using Tab Key inserts four spaces instead.
set expandtab
set tabstop=4
set shiftwidth=4

function! NumberToggle()
    if(&relativenumber == 1)
        set norelativenumber
    else
        set relativenumber          
    endif
endfunc

"NERDTree: CTRL-N toggles the NERDTree
noremap <C-b> :NERDTreeToggle()<CR>
vnoremap <C-b> <C-C>:NERDTreeToggle()<CR>
inoremap <C-b> <C-O>:NERDTreeToggle()<CR>

"Rebind all arrow keys to be non-functional. USE HJKL DAMMIT
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

"Set folding method to one based on indent.
set foldmethod=indent
set foldcolumn=1

"Rebind semicolon in normal mode into the functionality of colon.
"Also, double-semicolon should function as semicolon
map ; :
map ;; ;


noremap <C-n> :call NumberToggle()<cr>

nnoremap <silent> <F12> :bn<CR>
nnoremap <silent> <S-F12> :bp<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>

" Turn on omnicomplete
set omnifunc=syntaxcomplete#Complete

" Folding background color default is fucking awful.
:hi Folded ctermbg=232

" Use Desert Colorscheme
colorscheme desert

" Use Comma as leader key instead of backslash.
nnoremap , <NOP>
inoremap , <NOP>
vnoremap , <NOP>
let mapleader=","
